from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpRequest
from django.views.decorators.csrf import csrf_exempt
import json
from json import loads
from logging import getLogger
import random
from library.df_response_lib import *
from dialogflow_fulfillment import WebhookClient, QuickReplies


def home(request):
    return HttpResponse('Hello world')

@csrf_exempt
def webhook(request):
    # build request object
    req = json.loads(request.body)
    #get action from json
    action = req.get('queryResult')

    if action.get('action') == 'get_lugares_turisticos':
        paramLugar = str(action.get('parameters').get('LugaresTuristicos'))
        print(paramLugar)
        #Tikal
        if paramLugar == 'Tikal':
            reply = {
                "fulfillmentText": 'Tikal es uno de los mayores yacimientos arqueológicos y centros urbanos de la civilización maya precolombina, '
                                   'junto con Calakmul, Chichén Itzá y Palenque. Está situado en el municipio de Flores, en el departamento '
                                   'de Petén, en el territorio actual de la República de Guatemala y forma parte del parque nacional Tikal, '
                                   'que fue declarado Patrimonio de la Humanidad, por Unesco, en 1979.​ Según los glifos encontrados en el'
                                   ' yacimiento, su nombre maya habría sido Yax Mutul.​Tikal es uno de los centros turísticos más importantes '
                                   'en Guatemala. Tikal fue la capital de un estado beligerante, que se convirtió en uno de los reinos '
                                   'más poderosos de los antiguos mayas.​​Aunque la arquitectura monumental del sitio se remonta '
                                   'hasta el siglo iv a. C., Tikal alcanzó su apogeo durante el Período Clásico, entre el 200 y el 900 d. C. '
                                   'Durante este tiempo, la ciudad dominó gran parte de la región maya, en el ámbito político, económico y militar;'
                                   ' mantenía vínculos con otras regiones, a lo largo de Mesoamérica, incluso con la gran metrópoli de Teotihuacán, '
                                   'en el lejano Valle de México.​Después del Clásico Tardío, no se construyeron monumentos mayores'
,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Parque Nacional Tikal",
                            "subtitle": 'Tikal es uno de los mayores yacimientos arqueológicos y centros urbanos de la civilización maya precolombina, '
                                   'junto con Calakmul, Chichén Itzá y Palenque. Está situado en el municipio de Flores, en el departamento '
                                   'de Petén, en el territorio actual de la República de Guatemala y forma parte del parque nacional Tikal, '
                                   'que fue declarado Patrimonio de la Humanidad, por Unesco, en 1979.​ Según los glifos encontrados en el'
                                   ' yacimiento, su nombre maya habría sido Yax Mutul.​Tikal es uno de los centros turísticos más importantes '
                                   'en Guatemala. Tikal fue la capital de un estado beligerante, que se convirtió en uno de los reinos '
                                   'más poderosos de los antiguos mayas.​​Aunque la arquitectura monumental del sitio se remonta '
                                   'hasta el siglo iv a. C., Tikal alcanzó su apogeo durante el Período Clásico, entre el 200 y el 900 d. C. '
                                   'Durante este tiempo, la ciudad dominó gran parte de la región maya, en el ámbito político, económico y militar;'
                                   ' mantenía vínculos con otras regiones, a lo largo de Mesoamérica, incluso con la gran metrópoli de Teotihuacán, '
                                   'en el lejano Valle de México.​Después del Clásico Tardío, no se construyeron monumentos mayores',
                            "imageUri": "https://mcd.gob.gt/wp-content/uploads/2013/07/tikal8-300x200.jpg",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://mcd.gob.gt/tikal/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Antigua Guatemala
        if paramLugar == 'Antigua Guatemala':
            reply = {
                "fulfillmentText": 'La ciudad de Santiago de los Caballeros de Guatemala, cuyo nombre oficial e histórico es Muy Noble y Muy Leal Ciudad de Santiago de los Caballeros de Guatemala y popularmente nombrada en la actualidad como Antigua Guatemala, es cabecera del municipio homónimo y del departamento de Sacatepéquez, Guatemala; se ubica a aproximadamente 25 kilómetros al oeste de la capital de la República de Guatemala, y a una altitud de 1470 m s. n. m. Actualmente la ciudad se ubica dentro de las 20 ciudades más importantes de Guatemala.'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Antigua Guatemala",
                            "subtitle": 'La ciudad de Santiago de los Caballeros de Guatemala, cuyo nombre oficial e histórico es Muy Noble y Muy Leal Ciudad de Santiago de los Caballeros de Guatemala y popularmente nombrada en la actualidad como Antigua Guatemala, es cabecera del municipio homónimo y del departamento de Sacatepéquez, Guatemala; se ubica a aproximadamente 25 kilómetros al oeste de la capital de la República de Guatemala, y a una altitud de 1470 m s. n. m. Actualmente la ciudad se ubica dentro de las 20 ciudades más importantes de Guatemala.',
                            "imageUri": "https://content.r9cdn.net/rimg/dimg/e9/20/c95cd7db-city-40728-16c2090182d.jpg?crop=true&width=1366&height=768&xhint=2341&yhint=1775",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://laantiguaguatemala.org/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Basilica de Esquipulas
        if paramLugar == 'Basilica de Esquipulas':
            reply = {
                "fulfillmentText": 'La Iglesia Basílica Santuario del Santo Cristo Crucificado de Esquipulas o Basílica del Santo Cristo '
                                   'Crucificado de Esquipulas es un templo de estilo barroco ecléctico ubicado en la ciudad de Esquipulas, '
                                   'Guatemala, y que alberga a la venerada imagen del Señor de Esquipulas. Tiene estatus de Basílica menor y '
                                   'santuario católico Es considerado el principal recinto católico de Centroamérica y uno de los más visitados '
                                   'en el mundo, anualmente unos cinco millones y medio de peregrinos visitan el santuario, de los cuales '
                                   'cerca de un millón y medio lo hacen en los días cercanos al 15 de enero, día en que se festeja al Señor '
                                   'de Esquipulas y otros lo hacen el día 9 de marzo, día que se festeja la llegada de la imagen a la ciudad '
                                   'de Esquipulas, hecho sucedido el jueves 9 de marzo de 1595.'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Basilica del Santo Cristo",
                            "subtitle": 'La Iglesia Basílica Santuario del Santo Cristo Crucificado de Esquipulas o Basílica del Santo Cristo '
                                   'Crucificado de Esquipulas es un templo de estilo barroco ecléctico ubicado en la ciudad de Esquipulas, '
                                   'Guatemala, y que alberga a la venerada imagen del Señor de Esquipulas. Tiene estatus de Basílica menor y '
                                   'santuario católico Es considerado el principal recinto católico de Centroamérica y uno de los más visitados '
                                   'en el mundo, anualmente unos cinco millones y medio de peregrinos visitan el santuario, de los cuales '
                                   'cerca de un millón y medio lo hacen en los días cercanos al 15 de enero, día en que se festeja al Señor '
                                   'de Esquipulas y otros lo hacen el día 9 de marzo, día que se festeja la llegada de la imagen a la ciudad '
                                   'de Esquipulas, hecho sucedido el jueves 9 de marzo de 1595.',
                            "imageUri": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Esquipulas_basilica.JPG/280px-Esquipulas_basilica.JPG",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://aprende.guatemala.com/cultura-guatemalteca/patrimonios/basilica-de-esquipulas/"
                                }
                            ]
                        }
                    }
                ]
            }
        #HunnalYe
        if paramLugar == 'HunnalYe':
            reply = {
                "fulfillmentText": 'El Parque Ecológico Hun Nal Ye en Alta Verapaz es reconocido por ser un paraíso de aventura y naturaleza. '
                                   'Los visitantes pueden sumergirse y nadar en tranquilas pozas de agua turquesa.  Así como remar en kayak o '
                                   'bucear en el cenote. Hun Nal Ye cuenta con impresionantes atractivos naturales y una gran diversidad de '
                                   'actividades de aventura para hacer. Puedes disfrutar de: caminatas por los senderos, nadar en agua cristalina, '
                                   'acampar, canopy, montar a caballo, salto en garrucha, kayaking y bucear en el Cenote.'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "HunnalYe",
                            "subtitle": 'El Parque Ecológico Hun Nal Ye en Alta Verapaz es reconocido por ser un paraíso de aventura y naturaleza. '
                                   'Los visitantes pueden sumergirse y nadar en tranquilas pozas de agua turquesa.  Así como remar en kayak o '
                                   'bucear en el cenote. Hun Nal Ye cuenta con impresionantes atractivos naturales y una gran diversidad de '
                                   'actividades de aventura para hacer. Puedes disfrutar de: caminatas por los senderos, nadar en agua cristalina, '
                                   'acampar, canopy, montar a caballo, salto en garrucha, kayaking y bucear en el Cenote.',
                            "imageUri": "http://hunalye.com/wp-content/uploads/2020/08/parque-ecologico-hun-nal-ye-foto-013-1024x768-1.jpg",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "http://hunalye.com/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Semuc Champey
        if paramLugar == 'Semuc Champey':
            reply = {
                "fulfillmentText": 'Semuc Champey, es un enclave natural localizado en el municipio guatemalteco de ' \
                              'Lanquín, en el departamento de Alta Verapaz, Guatemala. En el mismo, en medio de un ' \
                              'espeso bosque tropical se halla un puente natural de piedra caliza de unos 300 metros de ' \
                              'largo por el cual fluye el río Cahabón y en cuyos alrededores se encuentran una gran cantidad ' \
                              'de pozas de 1 a 3 m de profundidad, cuyo color verde turquesa o color jade cambia a lo largo del ' \
                              'año variando con el clima, el sol y otros factores naturales. Al final puede observarse al río Cahabón ' \
                              'como se interna en una caverna de piedra caliza, área muy peligrosa para acercarse dentro del río por la ' \
                              'fuerza del agua, sólo se permite observar a distancia este fenómeno geográfico natural.​Semuc Champey ' \
                              'fue declarado en 1999 Monumento Natural por el entonces presidente de la República de Guatemala, ' \
                              'Álvaro Arzú Irigoyen y se encuentra muy próximo geográficamente al parque nacional Grutas de Lanquín ' \
                              'con el que comparte un centro de visitantes para dar cobertura al turista. Link'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Semuc Champey",
                            "subtitle": 'Semuc Champey, es un enclave natural localizado en el municipio guatemalteco de ' \
                              'Lanquín, en el departamento de Alta Verapaz, Guatemala. En el mismo, en medio de un ' \
                              'espeso bosque tropical se halla un puente natural de piedra caliza de unos 300 metros de ' \
                              'largo por el cual fluye el río Cahabón y en cuyos alrededores se encuentran una gran cantidad ' \
                              'de pozas de 1 a 3 m de profundidad, cuyo color verde turquesa o color jade cambia a lo largo del ' \
                              'año variando con el clima, el sol y otros factores naturales. Al final puede observarse al río Cahabón ' \
                              'como se interna en una caverna de piedra caliza, área muy peligrosa para acercarse dentro del río por la ' \
                              'fuerza del agua, sólo se permite observar a distancia este fenómeno geográfico natural.​Semuc Champey ' \
                              'fue declarado en 1999 Monumento Natural por el entonces presidente de la República de Guatemala, ' \
                              'Álvaro Arzú Irigoyen y se encuentra muy próximo geográficamente al parque nacional Grutas de Lanquín ' \
                              'con el que comparte un centro de visitantes para dar cobertura al turista. Link',
                            "imageUri": "https://aprende.guatemala.com/wp-content/uploads/2016/11/Parque-Nacional-Semuc-Champey-en-Guatemala4.jpg",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://aprende.guatemala.com/historia/geografia/parque-nacional-semuc-champey-en-guatemala/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Atitlan
        if paramLugar == 'Atitlan':
            reply = {
                "fulfillmentText": 'Atitlan'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Atitlan",
                            "subtitle": 'Atitlan',
                            "imageUri": "https://andeguat.org.gt/wp-content/uploads/2015/02/default-placeholder.png",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://turismogtbot.devkatu.com/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Quirigua
        if paramLugar == 'Quirigua':
            reply = {
                "fulfillmentText": 'Quirigua'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Quirigua",
                            "subtitle": 'Quirigua',
                            "imageUri": "https://andeguat.org.gt/wp-content/uploads/2015/02/default-placeholder.png",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://turismogtbot.devkatu.com/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Volcan de Ipala
        if paramLugar == 'Volcan de Ipala':
            reply = {
                "fulfillmentText": 'Volcan de Ipala'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Volcan de Ipala",
                            "subtitle": 'Volcan de Ipala',
                            "imageUri": "https://andeguat.org.gt/wp-content/uploads/2015/02/default-placeholder.png",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://turismogtbot.devkatu.com/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Volcan de Pacaya
        if paramLugar == 'Volcan de Pacaya':
            reply = {
                "fulfillmentText": 'El Volcán de Pacaya es un volcán activo ubicado en el municipio de Amatitlán en el departamento de Guatemala y el municipio de San Vicente Pacaya en el departamento de Escuintla, Guatemala. Se desconoce hace cuántos miles de años hizo erupción por primera vez, pero se han registrado al menos 23 erupciones desde la época de colonización española de América en Guatemala.​Después de estar dormido durante un siglo, hizo erupción violentamente en 1965 y desde entonces ha estado en constante actividad eruptiva. Mucha de su actividad es del tipo estromboliano y ocasionalmente pliniano.​El 20 de julio de 1963 fue declarado parque nacional y es un lugar de atracción turística extranjera y nacional. Está localizado a 47,5 km al sur de la Ciudad de Guatemala, puede visitarse desde la ciudad de Antigua Guatemala o la ciudad de Guatemala misma. Es uno de los volcanes de más recomendados por su fácil ascenso y hermosa vista'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Volcán de Pacaya",
                            "subtitle": 'El Volcán de Pacaya es un volcán activo ubicado en el municipio de Amatitlán en el departamento de Guatemala y el municipio de San Vicente Pacaya en el departamento de Escuintla, Guatemala. Se desconoce hace cuántos miles de años hizo erupción por primera vez, pero se han registrado al menos 23 erupciones desde la época de colonización española de América en Guatemala.​Después de estar dormido durante un siglo, hizo erupción violentamente en 1965 y desde entonces ha estado en constante actividad eruptiva. Mucha de su actividad es del tipo estromboliano y ocasionalmente pliniano.​El 20 de julio de 1963 fue declarado parque nacional y es un lugar de atracción turística extranjera y nacional. Está localizado a 47,5 km al sur de la Ciudad de Guatemala, puede visitarse desde la ciudad de Antigua Guatemala o la ciudad de Guatemala misma. Es uno de los volcanes de más recomendados por su fácil ascenso y hermosa vista',
                            "imageUri": "http://cdn.simplesite.com/i/5c/c3/282319407077114716/i282319414699620904._szw480h1280_.jpg",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://aprende.guatemala.com/historia/geografia/volcan-de-pacaya-guatemala/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Volcan de Fuego
        if paramLugar == 'Volcan de Fuego':
            reply = {
                "fulfillmentText": 'Volcan de Pacaya'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Volcan de Pacaya",
                            "subtitle": 'Volcan de Pacaya',
                            "imageUri": "https://andeguat.org.gt/wp-content/uploads/2015/02/default-placeholder.png",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://turismogtbot.devkatu.com/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Castillo de san felipe
        if paramLugar == 'Castillo de san felipe':
            reply = {
                "fulfillmentText": 'Castillo de san felipe'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Castillo de san felipe",
                            "subtitle": 'Castillo de san felipe',
                            "imageUri": "https://andeguat.org.gt/wp-content/uploads/2015/02/default-placeholder.png",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://turismogtbot.devkatu.com/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Rio dulce
        if paramLugar == 'Rio dulce':
            reply = {
                "fulfillmentText": 'Rio dulce'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Rio dulce",
                            "subtitle": 'Rio dulce',
                            "imageUri": "https://andeguat.org.gt/wp-content/uploads/2015/02/default-placeholder.png",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://turismogtbot.devkatu.com/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Isla de Flores
        if paramLugar == 'Isla de Flores':
            reply = {
                "fulfillmentText": 'Isla de Flores'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Isla de Flores",
                            "subtitle": 'Isla de Flores',
                            "imageUri": "https://andeguat.org.gt/wp-content/uploads/2015/02/default-placeholder.png",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://turismogtbot.devkatu.com/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Livingston
        if paramLugar == 'Livingston':
            reply = {
                "fulfillmentText": 'Livingston'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Livingston",
                            "subtitle": 'Livingston',
                            "imageUri": "https://andeguat.org.gt/wp-content/uploads/2015/02/default-placeholder.png",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://turismogtbot.devkatu.com/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Playa Dorada
        if paramLugar == 'Playa Dorada':
            reply = {
                "fulfillmentText": 'Playa Dorada'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Playa Dorada",
                            "subtitle": 'Playa Dorada',
                            "imageUri": "https://andeguat.org.gt/wp-content/uploads/2015/02/default-placeholder.png",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://turismogtbot.devkatu.com/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Puerto San Jose
        if paramLugar == 'Puerto San Jose':
            reply = {
                "fulfillmentText": 'Puerto San Jose'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Puerto San Jose",
                            "subtitle": 'Puerto San Jose',
                            "imageUri": "https://andeguat.org.gt/wp-content/uploads/2015/02/default-placeholder.png",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://turismogtbot.devkatu.com/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Irtra
        if paramLugar == 'Irtra':
            reply = {
                "fulfillmentText": 'Irtra'
                ,
                "fulfillmentMessages": [
                    {
                        "card": {
                            "title": "Irtra",
                            "subtitle": 'Irtra',
                            "imageUri": "https://andeguat.org.gt/wp-content/uploads/2015/02/default-placeholder.png",
                            "buttons": [
                                {
                                    "text": "Mas informacion",
                                    "postback": "https://turismogtbot.devkatu.com/"
                                }
                            ]
                        }
                    }
                ]
            }
        #Xetulul
        if paramLugar == 'Xetulul':
            reply = {
  "fulfillmentText": 'Xetulul: En el Gran Parque de Diversiones Xetulul encontrarás Magia y Diversión para toda la familia. '
                     'Descubre la gran variedad de actividades que puedes realizar. Disfruta de impresionantes atracciones '
                     'y emocionantes juegos electromecánicos para grandes y chicos, espectaculares shows ambulantes y el '
                     'fantástico Show de Magia del Gran Teatro de Francia, extraordinarias tiendas de regalos y diversidad '
                     'de restaurantes para todos los gustos en las diferentes plazas que brindan un escenario arquitectónico '
                     'donde se muestra el origen y la historia de Guatemala, así como los países europeos que han influenciado '
                     'la cultura guatemalteca.',
  "fulfillmentMessages": [
    {
      "card": {
        "title": "Xetulul",
        "subtitle": 'En el Gran Parque de Diversiones Xetulul encontrarás Magia y Diversión para toda la familia. '
                     'Descubre la gran variedad de actividades que puedes realizar. Disfruta de impresionantes atracciones '
                     'y emocionantes juegos electromecánicos para grandes y chicos, espectaculares shows ambulantes y el '
                     'fantástico Show de Magia del Gran Teatro de Francia, extraordinarias tiendas de regalos y diversidad '
                     'de restaurantes para todos los gustos en las diferentes plazas que brindan un escenario arquitectónico '
                     'donde se muestra el origen y la historia de Guatemala, así como los países europeos que han influenciado '
                     'la cultura guatemalteca.',
        "imageUri": "https://upload.wikimedia.org/wikipedia/commons/c/c9/Xetulul_Guatemala.jpg",
        "buttons": [
          {
            "text": "Mas informacion",
            "postback": "https://irtra.org.gt/parques/"
          }
        ]
      }
    }
  ]
}

    if action.get('action') == 'get_saludo':
        saludos = ['¡Hola! Bienvenid@ a TurismoGT-Bot','Bienvenid@ a TurismoGT-Bot','¡Hola! Escribe un lugar del que desees más información', 'Bienvenid@ a TurismoGT-Bot, ¿Qué lugar deseas conocer?','TurismoGT-Bot te saluda!! ¿De qué lugar deseas saber información?']
        reply = {'fulfillmentText': random.choice(saludos)}

    if action.get('action') == 'get_recomendacion_lugar':
        lugares = ['Te recomendamos: Tikal',
                   'Te recomendamos: Antigua Guatemala',
                   'Te recomendamos: Basilica de Esquipulas',
                   'Te recomendamos: HunnalYe',
                   'Te recomendamos: Semuc Champey'
                   'Te recomendamos: Atitlan',
                   'Te recomendamos: Quirigua',
                   'Te recomendamos: Volcan de Ipala',
                   'Te recomendamos: Volcan de Pacaya',
                   'Te recomendamos: Volcan de Fuego',
                   'Te recomendamos: Castillo de San Felipe',
                   'Te recomendamos: Rio Dulce',
                   'Te recomendamos: Isla de Flores',
                   'Te recomendamos: Livingston',
                   'Te recomendamos: Playa Dorada',
                   'Te recomendamos: Puerto San Jose',
                   'Te recomendamos: Irtra',
                   'Te recomendamos: Xetulul']
        reply = {'fulfillmentText': random.choice(lugares)}

    return JsonResponse(reply, safe=False)
    
